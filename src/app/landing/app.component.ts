import {Component, HostListener, ViewChild, ElementRef, Renderer2} from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
    @ViewChild('msFirst') el: ElementRef;
    shrink: boolean;
    vendorCategories: boolean;
    swing: boolean;

    constructor(private rd: Renderer2) {
        this.shrink = false;
        this.vendorCategories = false;
        this.swing = false;
    }

    showVendorCategories() {
        this.vendorCategories = !this.vendorCategories;


        setTimeout(() => {
            $(this.el.nativeElement).fadeIn(100);
        }, 50);

        setTimeout(() => {
            $('.ms-second').fadeIn(70);
        }, 200);

        setTimeout(() => {
            $('.ms-third').fadeIn(70);
        }, 350);
    }

    @HostListener('window:scroll', ['$event']) onWindowScroll(event) {
        if (window.pageYOffset > 50) {
          this.shrink = true;
        }else{
          this.shrink = false;
        }
    }
}
